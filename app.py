from flask import Flask, request, url_for, redirect

app = Flask(__name__)


@app.route('/')
def index():
    print(request.query_string)

    menu = f'''
        GO <a href="{url_for('exchange_new')}"> here</a> to exchange money<br>
        <img src="{url_for('static',filename='sample-image.jpg')}">
    '''
    return menu


@app.route('/cantor/<currency>/<int:amount>')
def cantor(currency, amount):
    message = f'{currency},{amount}'
    return message


@app.route('/exchange')
def exchange():

    body = '''
        <form id ="exchange_form" action="/exchange_process" method="POST">
        <label for="currency">Currency</label>
        <input type="text" id="currency" name="currency" value="EUR"><br>
        <input type="submit" value="Send">
        
        </form>
    '''
    return body


@app.route('/exchange_process', methods=['POST'])
def exchange_process():

    currency = 'PLN'
    if 'currency' in request.form:
        currency = request.form['currency']

    txt = 'LOL '+currency
    return txt


@app.route('/exchange_new', methods=['GET', 'POST'])
def exchange_new():

    if request.method == 'GET':
        body = '''
            <form id ="exchange_form" action="/exchange_new" method="POST">
            <label for="currency">Currency</label>
            <input type="text" id="currency" name="currency" value="EUR"><br>
            <input type="submit" value="Send">
            
            </form>
        '''
        return body
    else:
        currency = 'PLN'
        if 'currency' in request.form:
            currency = request.form['currency']

    txt = 'LOL '+currency
    return redirect(url_for('cantor', currency='XXX', amount=222))


@app.route('/about')
def about():
    return 'dupa'


if __name__ == '__main__':
    app.run()
